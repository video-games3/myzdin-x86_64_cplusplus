# Myzdin-x86_64_Cplusplus

## Gameplay
![](image/Myzdin_progress.jpg)

## More info about my Game
[Myzdin website](https://sagedemage.github.io/MyzdinWebsite/index.html)

## Building the project in Linux
[building project in linux](https://sagedemage.github.io/MyzdinWebsite/building_game/linux_build.html)

## Assets (Music, Art and Soundeffects)
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />I created the music, art and soundeffects for this game. The assets are licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
