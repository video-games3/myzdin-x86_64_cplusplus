#ifndef COLLISION_H
#define COLLISION_H

void ground_collision(Player& player, int ground_height);
void collision_soundeffect(Player& player, Mix_Chunk* landing_noise);

#endif // COLLISION_H
