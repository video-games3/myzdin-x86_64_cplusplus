#include "../level.h"
#include "../player/player.h"
#include "../ground/ground.h"

void ground_collision(Player& player, int ground_height) {
    /* Sets the player's condition when landing on the ground */

    int bottomP = player.getDstYPos() + player.getSrcHeight();

    if (bottomP >= LEVEL_HEIGHT - ground_height) {
        player.setDstYPos(LEVEL_HEIGHT - ground_height - player.getSrcHeight());
        player.setVerticalAccel(0);
    }
}

void collision_soundeffect(Player& player, Mix_Chunk* landing_noise) {
    /* Initiate ground collision sound effect */
    if (player.getYVelocity() == -player.getMaxVerticalSpeed() && player.getYAccel() == player.getMaxVerticalAccel()) {
        Mix_PlayChannel(1, landing_noise, 0);
    }
}
