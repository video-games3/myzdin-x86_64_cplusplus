#include "level.h"
#include "player/player.h"
#include "scene/scene.h"
#include "ground/ground.h"
#include "game_play/game_play.h"

// width and height of the window
constexpr int window_width = 750;
constexpr int window_height = 500;

// width and height of the screen
constexpr int screen_width = 300;
constexpr int screen_height = 200;

// height of the ground
constexpr int ground_height = 25;

// width and height of the player
constexpr int player_width = 51;
constexpr int player_height = 75;

// player movement
constexpr int spdx = 5; //horizontal speed
constexpr int spdy = 18; //vertical speed
constexpr int accely = 1; //vertical accleration

auto main () -> int {
	// Note: MIX_MAX_VOLUME=128
    const int music_volume = 18;
	const int sound_effect_volume = 48; //48
    const int chunksize = 1024;

    /* Paths to the assets of the game */
	const char *scene_path = "assets/art/scene.png";
	const char *player_path = "assets/art/spritesheet.png";
	const char *ground_path = "assets/art/ground.png";
	const char *music_path = "assets/music/lost.ogg";
	const char *landing_noise_path = "assets/soundeffects/landing_noise.wav";

    /* Initialize SDL, window, audio, and renderer */
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER); //Initilize SDL library

    SDL_GameController* gamecontroller = SDL_GameControllerOpen(0); //Open Game Controller

    // Create window
    SDL_Window* win = SDL_CreateWindow("Myzdin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, window_width, window_height, 0);

    // Set Fullscreen
    //SDL_SetWindowFullscreen(win, SDL_WINDOW_FULLSCREEN);

    Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, chunksize); // Initialize SDL mixer

    // Creates a renderer to render our images
    // * SDL_RENDERER_ACCELERATED starts the program using the GPU hardware
    SDL_Renderer* rend = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

    /* Loads images, music and soundeffects */
    // Creates the asset that loads the image into main memory
    SDL_Surface* SceneSurf = IMG_Load(scene_path); //scene
    SDL_Surface* PlayerSurf = IMG_Load(player_path); // player
    SDL_Surface* GroundSurf = IMG_Load(ground_path); // ground
    Mix_Music* music = Mix_LoadMUS(music_path); // Load audio file
    Mix_Chunk* landing_noise = Mix_LoadWAV(landing_noise_path); // Load wav file

    // Loads image to our graphics hardware memory
    SDL_Texture* SceneTex = SDL_CreateTextureFromSurface(rend, SceneSurf);
    SDL_Texture* PlayerTex = SDL_CreateTextureFromSurface(rend, PlayerSurf);
    SDL_Texture* GroundTex = SDL_CreateTextureFromSurface(rend, GroundSurf); 

    Mix_VolumeMusic(music_volume); // Adjust music volume
	Mix_Volume(1, sound_effect_volume); //set soundeffect volume
    Mix_PlayMusic(music, -1); //Start background music (-1 means infinity)

    Shape player_shape = {{0, 0, player_width, player_height}, {0, LEVEL_HEIGHT - player_height, player_width, player_height}};
    Shape ground_shape = {{0, 0, screen_width, ground_height}, {0, LEVEL_HEIGHT - ground_height, LEVEL_WIDTH, ground_height}};
    SDL_Rect scene_srcrect = {0, 0, screen_width, screen_height};

    Limits player_limits = {spdx, spdy, accely};

    // Instatntiate Player, Scene, and Ground objects
    Player player = Player(PlayerTex, player_shape, player_limits);
    Scene scene = Scene(SceneTex, scene_srcrect);
    Ground ground = Ground(GroundTex, ground_shape);

    GamePlay(rend, player, scene, ground, landing_noise, gamecontroller);  //Movement and Physics

    /* Free resources and close SDL and SDL mixer */
     // Dealocate player and scene surfaces
    SDL_FreeSurface(PlayerSurf);
    SDL_FreeSurface(SceneSurf);
    SDL_FreeSurface(GroundSurf);

    //Free the music
    Mix_FreeMusic(music);
    Mix_FreeChunk(landing_noise);

    //Destroy scene and player textures
    SDL_DestroyTexture(SceneTex);
    SDL_DestroyTexture(PlayerTex);
    SDL_DestroyTexture(GroundTex);

    // Destroy renderer
    SDL_DestroyRenderer(rend);

    // Destroy window
    SDL_DestroyWindow(win);

    SDL_GameControllerClose(gamecontroller); //Close Game Controller
    Mix_CloseAudio(); // Close Audio
    SDL_Quit(); // Quit SDL subsystems

    return 0;
}
