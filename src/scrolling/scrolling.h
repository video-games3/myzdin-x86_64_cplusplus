#ifndef SCROLLING_H
#define SCROLLING_H

void SceneScrolling(Scene& scene, Player& player);
void GroundScrolling(Ground& ground, Player& player, Scene& scene);

#endif // SCROLLING_H
