#include "../player/player.h"
#include "../scene/scene.h"
#include "../ground/ground.h"

void SceneScrolling(Scene& scene, Player& player) {
    scene.setSrcXPos((player.getDstXPos() + player.getSrcWidth() / 2 ) - scene.getSrcWidth()  / 2);
}

void GroundScrolling(Ground& ground, Player& player, Scene& scene) {
    ground.setSrcXPos((player.getDstXPos() + player.getSrcWidth() / 2 ) - scene.getSrcWidth() / 2);
}
