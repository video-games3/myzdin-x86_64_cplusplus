#include "../level.h"
#include "../player/player.h"
#include "../scene/scene.h"
#include "../ground/ground.h"
#include "boundaries.h"

void SceneBoundaries(Scene& scene) {
        /* Camera boundaries */
        // left boundary
        if (scene.getSrcXPos() < 0) {
            scene.setSrcXPos(0);
	}
        // right boundary
        if (scene.getSrcXPos() > LEVEL_WIDTH - scene.getSrcWidth()) {
            scene.setSrcXPos(LEVEL_WIDTH - scene.getSrcWidth());
	}
        // bottom boundary
        if (scene.getSrcYPos() > LEVEL_WIDTH - scene.getSrcHeight()) {
            scene.setSrcYPos(LEVEL_WIDTH - scene.getSrcHeight());
	}
        // top boundary
        if (scene.getSrcYPos() < 0) {
            scene.setSrcYPos(0);
	}
}

void GroundBoundaries(Ground& ground) {
    /* Camera boundaries */
    // left boundary
    if (ground.getSrcXPos() < 0) {
        ground.setSrcXPos(0);
	}
    // right boundary
    if (ground.getSrcXPos() > LEVEL_WIDTH - ground.getSrcWidth()) {
        ground.setSrcXPos(LEVEL_WIDTH - ground.getSrcWidth());
	}
    // bottom boundary
    if (ground.getSrcYPos() > LEVEL_WIDTH - ground.getSrcHeight()) {
        ground.setSrcYPos(LEVEL_WIDTH - ground.getSrcHeight());
	}
    // top boundary
    if (ground.getSrcYPos() < 0) {
        ground.setSrcYPos(0);
	}
}

void PlayerBoundaries(Player& player, Scene& scene) {
    const int level_offset = LEVEL_WIDTH - scene.getSrcWidth() - LEVEL_WIDTH/2; // level offset
    /* Player boundaries */
    // left boundary
    if (player.getDstXPos() - player.getSrcWidth()/2 < -(level_offset)) {
        player.setDstXPos(-(level_offset) + player.getSrcWidth()/2);
	}
    // right boundary
    if (player.getDstXPos() + player.getSrcWidth()/2 > LEVEL_WIDTH + level_offset) {
        player.setDstXPos(LEVEL_WIDTH + level_offset - player.getSrcWidth()/2);
	}
    // bottom boundary
    if (player.getDstYPos() + player.getSrcHeight() > LEVEL_HEIGHT) {
        player.setDstYPos(LEVEL_HEIGHT - player.getSrcHeight());
	}
    // top boundary
    if (player.getDstYPos() < 0) {
        player.setDstYPos(0);
	}
}
