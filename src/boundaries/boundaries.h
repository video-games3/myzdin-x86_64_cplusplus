#ifndef BOUNDARIES_H
#define BOUNDARIES_H

void SceneBoundaries(Scene& scene);
void GroundBoundaries(Ground& ground);
void PlayerBoundaries(Player& player, Scene& scene);

#endif //BOUNDARIES_H
