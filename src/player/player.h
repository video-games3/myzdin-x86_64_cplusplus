#ifndef PLAYER_H
#define PLAYER_H

#include "../shape/shape.h"

struct Limits {
	const int spdx; //horizontal speed
	const int spdy; //vertical speed
	const int accely; //vertical accleration
} __attribute__((aligned(16)));

struct SceneCoords {
	int xpos;
	int ypos;
} __attribute__((aligned(8)));

class Player {

	// Variables
	SDL_Texture* PlayerTex;
	SDL_Rect srcrect; // player source from the player sprite
	SDL_Rect dstrect; // player destination
	int vx = 0, vy = 0; // horizontal and vertical velocity
	int accely = 0; // player vertical acceleration
	bool move_ani = false; // move animation
	Uint32 ticks = 0; //timer
	int seconds = 0; //seconds
	int sprite = 0; //sprite
	bool jump_init = false; // initiate jump
	Limits player_limits; // player movement
	SDL_RendererFlip flip = SDL_FLIP_NONE; // player direction

	// Methods
	void MinHeight();

	public:
		// Constructor
		Player(SDL_Texture* PlayerTex, Shape player_shape, Limits player_limits);

		// Methods
		void Gravity();
		void WalkSpeed(int vx);
		void Jump();
		void Inertia();
		void Render(SDL_Renderer* rend, int scene_width, SceneCoords scene_coords);
		void SpriteAnimation();

		/* Getters */
		auto getSource() const -> SDL_Rect;
		auto getDstXPos() const -> int;
		auto getDstYPos() const -> int;
		auto getSrcWidth() const -> int;
		auto getSrcHeight() const -> int;
		auto getYVelocity() const -> int;
		auto getYAccel() const -> int;
		auto getTexture() const -> SDL_Texture*;
		auto getMaxHorizontalSpeed() const -> int;
		auto getMaxVerticalSpeed() const -> int;
		auto getMaxVerticalAccel() const -> int;

		/* Setters */
		void setDstXPos(int dst_x_pos);
		void setDstYPos(int dst_y_pos);
		void setVerticalAccel(int vertical_accel);
		void setFaceDirection(SDL_RendererFlip facing_direction);

		//Mix_Chunk* landing_noise;
};

#endif //PLAYER_H
