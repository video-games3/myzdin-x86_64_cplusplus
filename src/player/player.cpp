#include "player.h"

// Constructor
Player::Player(SDL_Texture* PlayerTex, Shape player_shape, Limits player_limits) :
PlayerTex{PlayerTex}, srcrect{player_shape.srcrect}, dstrect{player_shape.dstrect}, player_limits{player_limits} {}

// Methods
void Player::Gravity() {
    /* Implements the gravity */
    dstrect.y -= vy;
    vy -= accely;

	/* The player landed on the ground */
	if (vy == -player_limits.spdy) {
        jump_init = false;
	}
}

void Player::WalkSpeed(int vx) {
    /* Checks if the player is moving */
	this->vx = vx; //set horizontal velocity
	move_ani = true;
	if (vx == 0) {
    	move_ani = false;
	}
}

void Player::Jump() {
    /* Implements player jumping */
    if (!jump_init) {
        // jimp if on the ground
        jump_init = true;
        vy = player_limits.spdy; // set vertical velocity
        accely = player_limits.accely; // set vertical acceleration
    }
}

void Player::Inertia() {
    /* The player's movement depending on the player's speed */
	dstrect.x += vx;
}

void Player::Render(SDL_Renderer* rend, int scene_width, SceneCoords scene_coords) {
	/* Renders the player */
    const int x_offset = 10;
    //SDL_Rect srcrect = player.getSource();
    SDL_Rect incamera = {0, 0, srcrect.w, srcrect.h};
    incamera.x = (dstrect.x + srcrect.w - x_offset + scene_width/2) - scene_coords.xpos;
    incamera.y = dstrect.y - scene_coords.ypos;
	SDL_RenderCopyEx(rend, PlayerTex, &srcrect, &incamera, 0.0, nullptr, flip);
}

void Player::SpriteAnimation() {
    /* Animation of the player */
    int const animation_frames = 200;
    srcrect = { 0, 0, srcrect.w, srcrect.h };

    if (move_ani && !jump_init) { // walk animation
        ticks = SDL_GetTicks(); // 1000 miliseconds
        seconds = int(ticks) / animation_frames; // 200 fps
        sprite = seconds % 4;
        //srcrect = { sprite * srcrect.w, 0, srcrect.w, srcrect.h };
        srcrect.x = sprite * srcrect.w;
    }
    else if (jump_init) { // jump animation
        //srcrect = { srcrect.w * 3, 0, srcrect.w, srcrect.h };
        srcrect.x = srcrect.w * 3;
    }
}

/* Getters */
auto Player::getSource() const -> SDL_Rect {
    return srcrect;
}

auto Player::getDstXPos() const -> int {
	return dstrect.x;
}

auto Player::getDstYPos() const -> int {
	return dstrect.y;
}

auto Player::getSrcWidth() const -> int {
	return srcrect.w;
}

auto Player::getSrcHeight() const -> int {
	return srcrect.h;
}

auto Player::getYVelocity() const -> int {
    return vy;
}

auto Player::getYAccel() const -> int {
    return accely;
}

auto Player::getTexture() const -> SDL_Texture* {
    return PlayerTex;
}

auto Player::getMaxHorizontalSpeed() const -> int {
    return player_limits.spdx;
}

auto Player::getMaxVerticalSpeed() const -> int {
    return player_limits.spdy;
}

auto Player::getMaxVerticalAccel() const -> int {
    return player_limits.accely;
}

/* Setters */
void Player::setDstXPos(int dst_x_pos) {
	dstrect.x = dst_x_pos;
}

void Player::setDstYPos(int dst_y_pos) {
	dstrect.y = dst_y_pos;
}

void Player::setVerticalAccel(int vertical_accel) {
    accely = vertical_accel;
}

void Player::setFaceDirection(SDL_RendererFlip facing_direction) {
    flip = facing_direction;
}
