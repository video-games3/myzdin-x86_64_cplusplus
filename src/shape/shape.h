#ifndef SHAPE_H
#define SHAPE_H

struct Shape {
	SDL_Rect srcrect;
	SDL_Rect dstrect;
} __attribute__((aligned(32)));

#endif //SHAPE_H
