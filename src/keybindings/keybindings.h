#ifndef KEYBINDINGS_H
#define KEYBINDINGS_H

void HoldKeybindings(Player& player, SDL_GameController* gamecontroller);
void ClickKeybindings(Player& player, bool& quit);

#endif //KEYBINDINGS_H
