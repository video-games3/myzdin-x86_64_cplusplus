#include "../player/player.h"
#include "keybindings.h"

void HoldKeybindings(Player& player, SDL_GameController* gamecontroller) {
    /* Hold Key bindings */
    // Get the snapshot of the current state of the keyboard
    const Uint8* state = SDL_GetKeyboardState(nullptr);
	
	int speed = 0;
	int left_dpad = SDL_GameControllerGetButton(gamecontroller, SDL_CONTROLLER_BUTTON_DPAD_LEFT);
	int right_dpad = SDL_GameControllerGetButton(gamecontroller, SDL_CONTROLLER_BUTTON_DPAD_RIGHT);

    if (state[SDL_SCANCODE_A] == 1 || left_dpad == 1) { //left
		speed = -(player.getMaxHorizontalSpeed());
        player.setFaceDirection(SDL_FLIP_HORIZONTAL);
    }

    if (state[SDL_SCANCODE_D] == 1 || right_dpad == 1) { //right
		speed = player.getMaxHorizontalSpeed();
        player.setFaceDirection(SDL_FLIP_NONE);
    }

	player.WalkSpeed(speed);
}

void ClickKeybindings(Player& player, bool& quit) {
    /* Click Key Bindings */
    SDL_Event event; // Event handling

    #pragma unroll
    while (SDL_PollEvent(&event) == 1) { // Events management
        switch(event.type) {
		    case SDL_QUIT: // close button
                quit = true;
                break;
            case SDL_KEYDOWN: // key press
                if (event.key.keysym.scancode == SDL_SCANCODE_K && event.key.repeat == 0) {
                    player.Jump();
                }
                if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE) {
                    quit = true;
                }
                break;

            case SDL_CONTROLLERBUTTONDOWN: // controller button press
                if (event.cbutton.button == SDL_CONTROLLER_BUTTON_A) {
                    player.Jump();
                }
                if (event.cbutton.button == SDL_CONTROLLER_BUTTON_START) {
                    quit = true;
                }
            default:
                break;
        }
    }
}
