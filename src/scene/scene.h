#ifndef SCENE_H
#define SCENE_H

class Scene {
public:
	// Constructor
	Scene(SDL_Texture* SceneTex, SDL_Rect srcrect);
	void Render(SDL_Renderer* rend);
	auto getSrcXPos() const -> int;
	auto getSrcYPos() const -> int;
	auto getSrcWidth() const -> int;
	auto getSrcHeight() const -> int;
	void setSrcXPos(int camX);
	void setSrcYPos(int camY);

private:
	SDL_Texture* SceneTex;
	// The camera displays the scene view and it displays a portion of the game map.
	SDL_Rect srcrect;
	//SDL_Rect srcrect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};
};

#endif // SCENE_H
