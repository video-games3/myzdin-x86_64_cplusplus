#include "scene.h"

// Constructor
Scene::Scene(SDL_Texture* SceneTex, SDL_Rect srcrect) : SceneTex{SceneTex}, srcrect{srcrect} {
    /* Initilaize the player object */
}

// Methods
void Scene::Render(SDL_Renderer* rend) {
    /* Render the scene */
	SDL_RenderCopy(rend, SceneTex, &srcrect, nullptr); //render scene
}

auto Scene::getSrcXPos() const -> int {
    return srcrect.x;
}

auto Scene::getSrcYPos() const -> int {
    return srcrect.y;
}

auto Scene::getSrcWidth() const -> int {
    return srcrect.w;
}

auto Scene::getSrcHeight() const -> int {
    return srcrect.h;
}

void Scene::setSrcXPos(int camX) {
    srcrect.x = camX;
}

void Scene::setSrcYPos(int camY) {
    srcrect.y = camY;
}
