#include "ground.h"

Ground::Ground(SDL_Texture* GroundTex, Shape ground_shape) : GroundTex{GroundTex},
srcrect{ground_shape.srcrect}, dstrect{ground_shape.dstrect} {}

void Ground::Render(SDL_Renderer* rend) {
	/* Render the ground */
	SDL_RenderCopy(rend, GroundTex, &srcrect, &dstrect);
}

/* Getters */
auto Ground::getSrcXPos() const -> int {
	return srcrect.x;
}

auto Ground::getSrcYPos() const -> int {
	return srcrect.x;
}

auto Ground::getSrcWidth() const -> int {
	return srcrect.w;
}

auto Ground::getSrcHeight() const -> int {
	return srcrect.h;
}

/* Setters */
void Ground::setSrcXPos(int src_x_pos) {
	srcrect.x = src_x_pos;
}

void Ground::setSrcYPos(int src_y_pos) {
	srcrect.y = src_y_pos;
}
