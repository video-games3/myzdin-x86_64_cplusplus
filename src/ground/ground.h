#ifndef GROUND_H
#define GROUND_H

#include "../shape/shape.h"

class Ground {

	//SDL_Renderer* rend;
	SDL_Texture* GroundTex;
	/* This displays the ground view and it displays a portion of the ground */
	SDL_Rect srcrect; // ground source
	SDL_Rect dstrect; // ground destination


	public:
		// Constructor
		Ground(SDL_Texture* GroundTex, Shape ground_shape);
		void Render(SDL_Renderer* rend);
		/* Getters */
		auto getSrcXPos() const -> int;
		auto getSrcYPos() const -> int;
		auto getSrcWidth() const -> int;
		auto getSrcHeight() const -> int;
		/* Setters */
		void setSrcXPos(int src_x_pos);
		void setSrcYPos(int src_y_pos);
};

#endif // GROUND_H
