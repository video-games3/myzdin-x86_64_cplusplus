#ifndef LEVEL_H
#define LEVEL_H

// width and height of the level
constexpr int LEVEL_WIDTH = 1000;
constexpr int LEVEL_HEIGHT = 500;

#endif // LEVEL_H
