#ifndef GAME_PLAY_H
#define GAME_PLAY_H

void GamePlay(SDL_Renderer* rend, Player player, Scene scene, Ground ground,  Mix_Chunk* landing_noise, SDL_GameController* gamecontroller);

#endif //GAME_PLAY_H
