#include "../level.h"
#include "../player/player.h"
#include "../scene/scene.h"
#include "../ground/ground.h"
#include "../keybindings/keybindings.h"
#include "../boundaries/boundaries.h"
#include "../scrolling/scrolling.h"
#include "../collision/collision.h"
#include "game_play.h"

void GamePlay(SDL_Renderer* rend, Player player, Scene scene, Ground ground,  Mix_Chunk* landing_noise, SDL_GameController* gamecontroller) {
    /* Implements the gameplay */
    bool quit = false; // gameplay loop switch
    const int second = 1000; // 1000 ms in a 1s
    const int gameplay_frames = 60;
    SceneCoords scene_coords = {0, 0}; // scene coordinates

    // Reference of player and scene object
    Scene& rscene = scene;
    Player& rplayer = player;
    Ground& rground = ground;
    bool& rquit = quit;

    #pragma unroll
    while (!quit) { // gameplay loop
        HoldKeybindings(rplayer, gamecontroller); // Hold Keybindings
        ClickKeybindings(rplayer, rquit); // Click Keybindings

        /* Scrolling */
        SceneScrolling(scene, player);
        GroundScrolling(ground, player, scene);

        player.SpriteAnimation(); // Sprite Animation
        player.Gravity(); // Gravity
        player.Inertia(); // Scene Inertia

        /* Boundaries */
        SceneBoundaries(rscene); // Set Camera Boundaries
        GroundBoundaries(rground); // Set Ground Boundaries
        PlayerBoundaries(rplayer, rscene); // Set Player Boundaries

        /* Ground Collision */
        // Sets the player's condition when landing on ground
        ground_collision(rplayer, ground.getSrcHeight());
        collision_soundeffect(rplayer, landing_noise); // initiates collision soundeffect

        scene_coords = {scene.getSrcXPos(), scene.getSrcYPos()}; // update scene coordinates

        // Animate Frames
        scene.Render(rend); // Render scene
        ground.Render(rend); // Render ground
        player.Render(rend, scene.getSrcWidth(), scene_coords); // Render player
        SDL_RenderPresent(rend); // Triggers double buffers for multiple rendering
        SDL_Delay(second / gameplay_frames); // Calculates to 60 fps
    }
}
